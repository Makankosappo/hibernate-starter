package org.dmdev;

import org.dmdev.entity.*;
import org.dmdev.util.HibernateUtil;
import lombok.Cleanup;
import org.dmdev.entity.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;

class HibernateRunnerTest {

    @Test
    void checkHql(){
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();

            var result = session.createQuery("select u from User u where u.personalInfo.firstname = :name", User.class)
                    .setParameter("name","ivan")
                    .list();
            System.out.println();
        }
    }

    @Test
    void checkInheritance(){
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();

/*            var google = Company.builder()
                    .name("Tales")
                    .build();

            session.save(google);

            var programmer = Programmer.builder()
                    .username("ivan@gmail.com")
                    .company(google)
                    .language(Language.JAVA)
                    .build();

            session.save(programmer);

            var manager = Manager.builder()
                    .username("ivan02@gmail.com")
                    .company(google)
                    .projectName("JAVA tutorial")
                    .build();

            session.save(manager);

            session.flush();

            session.clear();

            var prog1 = session.get(Programmer.class, 1L);

            var man1 = session.get(User.class, 2L);
            System.out.println();*/

            session.getTransaction().commit();
        }
    }

    @Test
    void checkH2(){
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();

            var exceptedCompany = Company.builder()
                    .name("Tales")
                    .build();

            session.save(exceptedCompany);

            session.getTransaction().commit();

            session.beginTransaction();

            var actualCompany = session.get(Company.class, 1L);

            Assertions.assertEquals(exceptedCompany,actualCompany);
        }
    }

    @Test
    void checkFlyWay() {
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();

            var exceptedCompany = Company.builder()
                    .name("Tales")
                    .build();
            session.save(exceptedCompany);

            session.getTransaction().commit();

            session.beginTransaction();

            var actualCompany = session.get(Company.class, 1L);

            Assertions.assertEquals(exceptedCompany,actualCompany);
        }
    }

    @Test
    void checkUsers() {
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();

            var company = session.get(Company.class, 5L);

            company.getUsers().forEach((k, v) -> System.out.println(v));

            session.getTransaction().commit();
        }
    }

    @Test
    void localeInfo() {
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();

            var company = session.get(Company.class, 5L);
            company.getLocaleInfos().add(LocaleInfo.of("ua", "Опис"));
            company.getLocaleInfos().add(LocaleInfo.of("en", "description"));

            session.getTransaction().commit();
        }
    }

    @Test
    void checkManyToOne() {
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();


            var user = session.get(User.class, 10L);
            var chat = session.get(Chat.class, 1L);

            var userChat = UserChat.builder()
                    .created_at(Instant.now())
                    .created_by(user.getUsername())
                    .build();

            userChat.setUser(user);
            userChat.setChat(chat);

            session.save(userChat);

            session.getTransaction().commit();
        }
    }

    @Test
    void checkOneToOne() {
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {
            session.beginTransaction();

/*            var user = User.builder()
                    .personalInfo(
                            PersonalInfo.builder()
                                    .firstname("Jack")
                                    .lastname("Jet")
                                    .build())
                    .username("muramasa@gmail.com")
                    .build();

            session.save(user);*/

            session.getTransaction().commit();


        }
    }

    @Test
    void checkOrphanRemoval() {
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {

            session.beginTransaction();

            var company = session.get(Company.class, 5);
            //            company.getUsers().removeIf(user -> user.getId().equals(2L));


            session.getTransaction().commit();

        }
    }

    @Test
    void checkLazyInitialisation() {
        Company company = null;
        try (var sessionFactory = HibernateUtil.buildSessionFactory();
             var session = sessionFactory.openSession()) {

            session.beginTransaction();

            company = session.get(Company.class, 5);


            session.getTransaction().commit();

        }
        var users = company.getUsers();
        System.out.println(users.size());
    }

    @Test
    void deleteCompany() {
        @Cleanup var sessionFactory = HibernateUtil.buildSessionFactory();
        @Cleanup var session = sessionFactory.openSession();
        session.beginTransaction();

        var user = session.get(User.class, 6L);
        session.delete(user);

        session.getTransaction().commit();
    }


    @Test
    void addUserToNewCompany() {
        @Cleanup var sessionFactory = HibernateUtil.buildSessionFactory();
        @Cleanup var session = sessionFactory.openSession();
        session.beginTransaction();

        var company = Company.builder()
                .name("Facebook")
                .build();

/*        var user = User.builder()
                .username("Sveta")
                .build();

        company.addUser(user);*/

        session.save(company);

        session.getTransaction().commit();
    }

    @Test
    void OneToMany() {
        @Cleanup var sessionFactory = HibernateUtil.buildSessionFactory();
        @Cleanup var session = sessionFactory.openSession();
        session.beginTransaction();

        var company = session.get(Company.class, 5);
        System.out.println(company.getUsers());

        session.getTransaction().commit();
    }

    @Test
    void checkGerReflectionApi() throws SQLException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException, NoSuchFieldException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = preparedStatement.executeQuery();
        resultSet.getString("username");
        resultSet.getString("firstname");
        resultSet.getString("lastname");

        Class<User> clazz = User.class;
        Constructor<User> constructor = clazz.getConstructor();
        User user = constructor.newInstance();

        Field usernameField = clazz.getDeclaredField("username");
        usernameField.setAccessible(true);
        usernameField.set(user, resultSet.getString("username"));
    }

    @Test
    void checkReflectionApi() throws SQLException, IllegalAccessException {
/*        User user = User.builder()
                .build();

        String sql = """
                insert
                into
                %s
                (%s)
                values
                (%s)
                """;

        String tableName = Optional.ofNullable(user.getClass().getAnnotation(Table.class))
                .map(tableAnotation -> tableAnotation.schema() + "." + tableAnotation.name())
                .orElse(user.getClass().getName());

        Field[] declaredFields = user.getClass().getDeclaredFields();

        String columnName = Arrays.stream(declaredFields)
                .map(field -> Optional.ofNullable(field.getAnnotation(Column.class))
                        .map(Column::name)
                        .orElse(field.getName()))
                .collect(Collectors.joining(", "));

        String columnValues = Arrays.stream(declaredFields)
                .map(field -> "?")
                .collect(Collectors.joining(", "));

        System.out.println(sql.formatted(tableName, columnName, columnValues));

        Connection connection = null;
        PreparedStatement preparedStatement = connection.prepareStatement(sql.formatted(tableName, columnName, columnValues));
        for (Field declaredField : declaredFields) {
            declaredField.setAccessible(true);
            preparedStatement.setObject(1, declaredField.get(user));*/
        }
    }
