package org.dmdev.entity;

import jakarta.persistence.*;
import lombok.*;
import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;

import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "name")
@ToString(exclude = "users")
@Builder
@Entity
@Audited
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String name;

    @NotAudited
    @Builder.Default
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
    @MapKey(name = "username")
//    @SortNatural
//    @OrderBy("username DESC, personalInfo.lastname ASC ")  // jakarta
//    @org.hibernate.annotations.OrderBy(clause = "username DESC, lastname ASC")  // hibernate
    private Map<String, User> users = new HashMap<>();

    @NotAudited
    @Builder.Default
    @ElementCollection
    @CollectionTable(name = "company_locale")
    private List<LocaleInfo> localeInfos = new ArrayList<>();

    public void addUser(User user){
        users.put(user.getUsername(), user);
        user.setCompany(this);
    }
}
