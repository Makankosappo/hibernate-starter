package org.dmdev.Dao;

import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;
import org.dmdev.entity.*;

import lombok.NoArgsConstructor;
import org.hibernate.Session;

import java.util.Collections;
import java.util.List;


@NoArgsConstructor
public class UserDao {

    private static final UserDao INSTANCE = new UserDao();

    public List<User> findAll(Session session) {
        return session.createQuery("select u from User u", User.class)
                .list();


/*        return new JPAQuery<User>(session)
                .select(QUser.user)
                .from(QUser.user)
                .fetch();*/

    }


    public List<User> findAllByFirstName(Session session, String firstName) {
        return session.createQuery("select u from User u " +
                "where u.personalInfo.firstname = :firstName", User.class)
                .setParameter("firstName", firstName)
                .list();

/*        return new JPAQuery<User>(session)
                .select(QUser.user)
                .from(QUser.user)
                .where(QUser.user.personalInfo.firstname.eq(firstName))
                .fetch();*/

    }


    public List<User> findLimitedUsersOrderedByBirthday(Session session, int limit) {
        return session.createQuery("select u from User u order by u.personalInfo.birthDate", User.class)
                .setMaxResults(limit)
                .list();

/*        return new JPAQuery<User>(session)
                .select(QUser.user)
                .from(QUser.user)
                .orderBy(QUser.user.personalInfo.birthDate.asc())
                .limit(limit)
                .fetch();*/
    }


    public List<User> findAllByCompanyName(Session session, String companyName) {
        return session.createQuery("select u from Company c " +
                        "join c.users u " +
                        "where c.name = :companyName", User.class)
                .setParameter("companyName", companyName)
                .list();

/*        return new JPAQuery<User>(session)
                .select(QUser.user)
                .from(QCompany.company)
                .join(QCompany.company.users, QUser.user)
                .where(QCompany.company.name.eq(companyName))
                .fetch();*/
    }


    public List<Payment> findAllPaymentsByCompanyName(Session session, String companyName) {
        return session.createQuery("select p from Payment p " +
                        "join p.receiver u " +
                        "join u.company c " +
                        "where c.name = :companyName " +
                        "order by u.personalInfo.firstname, p.amount", Payment.class)
                .setParameter("companyName", companyName)
                .list();
/*        return new JPAQuery<Payment>(session)
                .select(QPayment.payment)
                .from(QPayment.payment)
                .join(QPayment.payment.receiver, QUser.user)
                .join(QUser.user.company, QCompany.company)
                .where(QCompany.company.name.eq(companyName))
                .orderBy(QUser.user.personalInfo.firstname.asc(), QPayment.payment.amount.asc())
                .fetch();*/
    }


    public Double findAveragePaymentAmountByFirstAndLastNames(Session session, String firstName, String lastName) {
        return session.createQuery("select avg(p.amount) from Payment p " +
                        "join p.receiver u " +
                        "where u.personalInfo.firstname = :firstName " +
                        "   and u.personalInfo.lastname = :lastName", Double.class)
                .setParameter("firstName", firstName)
                .setParameter("lastName", lastName)
                .uniqueResult();

/*        return new JPAQuery<Double>(session)
                .select(QPayment.payment.amount.avg())
                .from(QPayment.payment)
                .join(QPayment.payment.receiver, QUser.user)
                .where(QUser.user.personalInfo.firstname.eq(firstName), QUser.user.personalInfo.lastname.eq(lastName))
                .fetchOne();*/
    }


    public List<Tuple> findCompanyNamesWithAvgUserPaymentsOrderedByCompanyName(Session session) {
/*        return session.createQuery("select c.name, avg(p.amount) from Company c " +
                        "join c.users u " +
                        "join u.payments p " +
                        "group by c.name " +
                        "order by c.name", Object[].class)
                .list();*/

/*        return new JPAQuery<Tuple>(session)
                .select(QCompany.company.name, QPayment.payment.amount.avg())
                .from(QCompany.company)
                .join(QCompany.company.users, QUser.user)
                .join(QUser.user.payments, QPayment.payment)
                .groupBy(QCompany.company.name)
                .orderBy(QCompany.company.name.asc())
                .fetch();*/
        return Collections.emptyList();
    }


    public List<Tuple> isItPossible(Session session) {
/*        return session.createQuery("select u, avg(p.amount) from User u " +
                        "join u.payments p " +
                        "group by u " +
                        "having avg(p.amount) > (select avg(p.amount) from Payment p) " +
                        "order by u.personalInfo.firstname", Object[].class)
                .list();*/

/*        return new JPAQuery<Tuple>(session)
                .select(QUser.user, QPayment.payment.amount.avg())
                .from(QUser.user)
                .join(QUser.user.payments, QPayment.payment)
                .groupBy(QUser.user.id)
                .having(QPayment.payment.amount.avg().gt(
                        new JPAQuery<Double>(session)
                                .select(QPayment.payment.amount.avg())
                                .from(QPayment.payment)
                ))
                .orderBy(QUser.user.personalInfo.firstname.asc())
                .fetch();*/

        return Collections.emptyList();
    }

    public static UserDao getInstance() {
        return INSTANCE;
    }
}
