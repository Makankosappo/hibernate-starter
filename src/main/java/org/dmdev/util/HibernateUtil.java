package org.dmdev.util;

import lombok.experimental.UtilityClass;
import org.dmdev.converter.BirthdayConverter;
import org.dmdev.entity.Audit;
import org.dmdev.interceptor.GlobalInterceptor;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@UtilityClass
public class HibernateUtil {

    //This method returns sessionFactory
    public static SessionFactory buildSessionFactory(){

        Configuration configuration = buildConfiguration();
        configuration.configure();

        return configuration.buildSessionFactory();
    }

    public static Configuration buildConfiguration(){
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(Audit.class);
        configuration.addAttributeConverter(new BirthdayConverter());
        configuration.setInterceptor(new GlobalInterceptor());
        return configuration;
    }
}
