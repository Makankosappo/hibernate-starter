package org.dmdev;

import jakarta.persistence.LockModeType;
import jakarta.transaction.Transactional;
import org.dmdev.interceptor.GlobalInterceptor;
import org.dmdev.util.HibernateUtil;
import org.dmdev.util.TestDataImporter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import lombok.extern.slf4j.Slf4j;
import org.dmdev.entity.*;
import org.hibernate.graph.GraphSemantic;

import java.sql.SQLException;
import java.util.Map;

@Slf4j
public class HibernateRunner {
    @Transactional
    public static void main(String[] args) throws SQLException {
        try (SessionFactory sessionFactory = HibernateUtil.buildSessionFactory()){
            Session session = sessionFactory.getCurrentSession();
                session.beginTransaction();



                session.getTransaction().commit();
        }
    }
}