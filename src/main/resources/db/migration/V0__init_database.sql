alter table if exists users
    drop constraint if exists FK_message_user;

drop table if exists users cascade;

alter table if exists company_locale
    drop constraint if exists FK_company_locale_user;


alter table if exists Profile
    drop constraint if exists FK_profile_user;

alter table if exists users_chat
    drop constraint if exists FK_users_chat_chat;

alter table if exists users_chat
    drop constraint if exists FK_users_chat_user;

drop table if exists Chat cascade;

drop table if exists Company cascade;

drop table if exists company_locale cascade;

drop table if exists Profile cascade;

drop table if exists users_chat cascade;

create table users
(
    id         BIGSERIAL not null,
    birth_date date,
    firstname  varchar(128),
    lastname   varchar(128),
    role       varchar(32),
    username   varchar(128) unique,
    company_id int,
    primary key (id)
);


create table Chat
(
    id   BIGSERIAL   not null,
    name varchar(32) not null unique,
    primary key (id)
);

create table Company
(
    id   serial      not null,
    name varchar(64) not null unique,
    primary key (id)
);


create table company_locale
(
    company_id  int        not null,
    description varchar(128),
    lang        varchar(2) not null,
    primary key (company_id, lang)
);


create table Profile
(
    id       BIGSERIAL not null,
    lang char(2),
    street   varchar(128),
    user_id  BIGINT    not null unique,
    primary key (id)
);


create table users_chat
(
    id         BIGSERIAL    not null,
    created_at timestamp(6) not null,
    created_by varchar(255) not null,
    chat_id    BIGINT,
    user_id    BIGINT,
    primary key (id)
);


alter table if exists users
    add constraint FK_message_user
        foreign key (company_id)
            references Company;

alter table if exists company_locale
    add constraint FK_company_locale_user
        foreign key (company_id)
            references Company;

alter table if exists Profile
    add constraint FK_profile_user
        foreign key (user_id)
            references users;

alter table if exists users_chat
    add constraint FK_users_chat_chat
        foreign key (chat_id)
            references Chat;

alter table if exists users_chat
    add constraint FK_users_chat_user
        foreign key (user_id)
            references users;